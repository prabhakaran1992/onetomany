package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.CourseModel;

public interface CourseRepo extends JpaRepository <CourseModel,Long>  {

}
