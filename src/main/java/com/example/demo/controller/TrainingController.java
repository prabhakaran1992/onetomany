package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.model.TrainingModel;
import com.example.demo.repo.TrainingRepo;

@Controller
public class TrainingController {
	
	@Autowired 
	TrainingRepo trainingrepo;
	
	
	@RequestMapping(value = "/create/training", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Map<String,TrainingModel> training(@RequestBody TrainingModel trainingmodel) {
		Map<String, TrainingModel> response = new HashMap<>();
		trainingrepo.save(trainingmodel);
		response.put("data", trainingmodel);
		return response;
	}
	@RequestMapping(value = "/datalist/training", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<TrainingModel>> details(HttpServletResponse response1
) 
	{
 		Map<String, List<TrainingModel>> response = new HashMap<>();
		List<TrainingModel> userdetails = new ArrayList<>();
		userdetails = trainingrepo.findAll();
		response.put("data", userdetails);
		return response;
	}
	
	

	@RequestMapping(value = "/deletetraining", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteusers(@RequestParam(name = "id") String id) {
		trainingrepo.delete(trainingrepo.findById(Long.parseLong(id)).get());
		return "course";
	}
	

}
