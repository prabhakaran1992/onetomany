package com.example.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.example.demo.model.CourseModel;
import com.example.demo.repo.CourseRepo;

@Controller
public class CourseController {
	@Autowired
	CourseRepo courseRepo;
	
	@RequestMapping(value = "/create/course", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public Map<String,CourseModel> signup(@RequestBody CourseModel user/* ,@RequestParam(name="rolename") String rolename */) {
		Map<String, CourseModel> response = new HashMap<>();
		courseRepo.save(user);
		response.put("data", user);
		return response;
	}
	
	@RequestMapping(value = "/datalist/course", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<CourseModel>> details(HttpServletResponse response1
) 
	{
 		Map<String, List<CourseModel>> response = new HashMap<>();
		List<CourseModel> userdetails = new ArrayList<>();
		/* Page<Users> page = Listall(pageable); */
		userdetails = courseRepo.findAll();

		response.put("data", userdetails);
		return response;
	}
	
	@RequestMapping(value = "/deletecourse", method = RequestMethod.DELETE)
	@ResponseBody
	public String deleteusers(@RequestParam(name = "id") String id) {
		courseRepo.delete(courseRepo.findById(Long.parseLong(id)).get());
		return "course";
	}
	
//	 @RequestMapping(value = "/editclinics",   method = {RequestMethod.POST,RequestMethod.GET})
//	  @ResponseBody 
//	  public CourseModel editIngredient( CourseModel clinics) {
////		  Optional<Clinics> userdetails =clinicsrepo.findById(clinics.getId());
//		 CourseModel clinic1 = courseRepo.findById(clinics.getId()).get();
//		  clinic1.setAddress1(clinics.getAddress1());
//		  clinic1.setAddress2(clinics.getAddress2());
//		  clinic1.setPhones(clinics.getPhones());
//		  clinic1.setCity(clinics.getCity());
//		  clinic1.setClinicname(clinics.getClinicname());
//		  clinic1.setWeburl(clinics.getWeburl());
//		  clinic1.setShortcode(clinics.getShortcode());
//		  clinic1.setLocality(clinics.getLocality());  
//		  courseRepo.save(clinic1);
//		  return course;
//	  }
}
