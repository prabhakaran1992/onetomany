package com.example.demo.model;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table

public class TrainingModel  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	
	public long id;
	public String firstname;
	public String lastname;
	public String phone;
	public String address;
	public String email;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "fkcourse",referencedColumnName = "id")
	    private CourseModel course;
	
	


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public CourseModel getCourse() {
		return course;
	}


	public void setCourse(CourseModel course) {
		this.course = course;
	}


	public TrainingModel() {
		super();
	}


	@Override
	public String toString() {
		return "TrainingModel [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", phone=" + phone
				+ ", address=" + address + ", email=" + email + ", course=" + course + "]";
	}
	
	
	
	
}
