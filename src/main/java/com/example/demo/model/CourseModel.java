package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class CourseModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	public long id;
	public String title;
	public String descriptoin;
	
	
	
	public long getId()
	
	{
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescriptoin() {
		return descriptoin;
	}
	public void setDescriptoin(String descriptoin) {
		this.descriptoin = descriptoin;
	}
	public CourseModel() {
		super();
	}
	@Override
	public String toString() {
		return "CourseModel [id=" + id + ", title=" + title + ", descriptoin=" + descriptoin + "]";
	}
	
	
	

}
